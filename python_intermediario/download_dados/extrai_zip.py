import os
import zipfile
import sys


def main(path):
    if not os.path.exists(path):
        print(f'Arquivo {path} não encontrado')
        sys.exit(-1)
    else:
        try:
            banco_zip = zipfile.ZipFile(path)
            print("Arquivos extraídos")
        except (FileNotFoundError, PermissionError):
            print("Algum problema ao ler op arquivo")
        else:
            banco_zip.extractall()
        finally:
            banco_zip.close()


if __name__ == "__main__":
    main(sys.argv[1])