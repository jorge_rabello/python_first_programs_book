from domain import *

print(DataTable.__doc__)
print(DataTable.__init__.__doc__)

table = DataTable('Alerta')

tabela_teste = DataTable(name='TabelaTeste')
print(tabela_teste.data)

table_empreendimento = DataTable("Empreendimento")
col_id = table_empreendimento.add_column('IdEmpreendimento', 'bigint')
col_aditivo = table_empreendimento.add_column('IdAditivo', 'bigint')
col_alerta = table_empreendimento.add_column('IdAlerta', 'bigint')

table_aditivo = DataTable("Aditivo")
col_id = table_aditivo.add_column('IdAditivo', 'bigint')
col_emp_id = table_aditivo.add_column('IdEmprendimento', 'bigint')

table_empreendimento.add_column('IdEmpreendimento', 'bigint')
table_empreendimento.add_references("IdAditivo", table_aditivo, col_aditivo)

table_aditivo.add_referenced("IdEmpreendimento", table_empreendimento,
                             col_emp_id)

print(Column('IdEmpreendimento', 'bigint'))
print(PrimaryKey(table_empreendimento, 'IdEmpreendimento', 'bigint'))

print(Column.validate('bigint', 10))
print(Column.validate('bigint', 'A'))

atable = DataTable("Empreedimento")
print(atable.name)

atable.name = 'Alerta'

del table.name