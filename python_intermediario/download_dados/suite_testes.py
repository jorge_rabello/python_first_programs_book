import unittest

HTML = '''\
<html>
    <head>
        <title>Unittest output</title>
    </head>
    <body>
        <table>{}</table>
    </body>
</html>'''

OK_TD = '<tr><td style="color: green;">{}</td></tr>'
ERR_TD = '<tr><td style="color: red;">{}</td></tr>'


class HTMLTestResult(unittest.TestResult):
    def __init__(self, runner):
        unittest.TestResult.__init__(self)
        self.runner = runner
        self.infos = []
        self.current = {}

    def new_test(self):
        self.infos.append(self.current)
        self.current = {}

    def start_test(self, test):
        self.current['id'] = test.id()

    def add_success(self, test):
        self.current['result'] = 'ok'
        self.new_test()

    def add_error(self, test, err):
        self.current['result'] = 'error'
        self.new_test()

    def add_failure(self, test, err):
        self.current['result'] = 'fail'
        self.new_test()

    def add_skip(self, test, err):
        self.current['result'] = 'skipped'
        self.current['reason'] = err
        self.new_test()


class HTMLTestRunner:
    def run(self, test):
        result = HTMLTestResult(self)
        test.run(result)
        table = ''
        for item in result.infos:
            if item['result'] == 'ok':
                table += OK_TD.format(item['id'])
            else:
                table += ERR_TD.format(item['id'])
        print(HTML.format(table))
        return result


if __name__ == "__main__":
    suite = unittest.TestLoader().discover('.')
    HTMLTestRunner().run(suite)