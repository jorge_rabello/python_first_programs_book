print(3 + 2)  #5
print(3 + 4.2)  #7.2
print(4 / 2)  #2.0
print(5 / 2)  #2.5
print(5 // 2)  #2
print(complex(1, 2) + 2)  #(3+2j)
print(complex(2, 0) + 0 + 1j)  #(2+1j)
print(2 + 0 + 1j)  #(2+1j)

# operadores aritméticos
# adição                                +
# subtração                             -
# multiplicação                         *
# divisão                               /
# resto de divisão                      %
# negativo                              -
# divisão descartante casas decimais   //
# potenciação                          **

print(1 + 2)  # 3
print(3 - 1)  # 2
print(10 / 2)  # 5.0
print(10 // 3)  # 3
print(10 * 2 + 1)  # 21
print(10 % 3)  # 1
print(-3)  # -3
print(2**8)  # 256

# operadores de bits
# ou x | y
# ou exclusivo x ^ y
# e x & y
# x << y x com y bits deslocados à esquerda
# x >> y x com y bits deslocados à direita
# inverso em bits ~x
print(1 | 0)  #  1
print(1 | 5)  #  5
print(1 ^ 5)  #  4
print(4 ^ 1)  #  5
print(1 << 2)  #  4
print(4 >> 2)  #  1
print(~4)  # -5

# coerção de tipo
# preço mais 30%
print(100 * 1.3)  # 130

print(type(1 + 2.0))  # <class 'float'>
print(type(1 + 2J))  # <class 'complex'>
print(type(1.0 + 2J))  # <class 'complex'>
print(type(1.0 + 1.0))  # <class 'float'>
print(type(1.0 + 1.0))  # <class 'float'>
