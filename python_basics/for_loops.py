impostos = ['MEI', 'Simples']

for imposto in impostos:
    print(imposto)

for imposto in impostos:
    if imposto.startswith("S"):
        continue
    print(imposto)

numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

for i in numeros:
    print(i)

print()
for i in range(5):
    print(i)
print(range(5))

novos_impostos = ["MEI", "Simples"]
for i, imposto in enumerate(novos_impostos):
    print(i, imposto)