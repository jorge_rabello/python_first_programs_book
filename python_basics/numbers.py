# Variáveis
idade = 32
preco = 34.60
valor = 1.0
complex_number = 1 + 2j

# Tipos de Dados
print(type(idade))  # <class 'int'>
print(type(preco))  # <class 'float'>
print(type(complex_number))  # <class 'complex'>

# Gerando números por built in functions
print(int(1.0))  # 1
print(int('9'))  # 9
print(float('9.2'))  # 9.2
print(float('-inf'))  # -inf
print(float('nan'))  # nan
print(complex(1, 2))  # (1+2j)
