salario = int(input('Digite o salario: '))
imposto = input('Digite o imposto em % (ex: 27.5): ')

if not imposto:
    imposto = 27.5
else:
    imposto = float(imposto)

print(f'Valor real {salario - (salario * imposto * 0.01)}')
