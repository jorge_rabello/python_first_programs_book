example_one = "Copa 2014"
example_two = 'Copa do mundo 2014'
example_three = '''2014 - Copa do mundo
                '''
example_four = "copa 'padrão fifa'"
example_five = 'copa "padrão fifa"'

print(example_one)  # Copa 2014
print(example_two)  # Copa do mundo 2014
print(example_three)  # 2014 - Copa do mundo e pula uma linha
print(example_four)  # copa 'padrão fifa'
print(example_five)  #  copa "padrão fifa"

print("""\
Uso: consulta_base [OPCOES]
      -h                             Exibe saída de ajuda
      -U                             URL do dataset
""")

print("Copa" "2014")

input('Em qual cidade o legado da Copa foi mais relevante '
      'para a população ? ')

estadio = "maracana"
print(estadio[0])  # m
print(estadio[1:4])  # ara
print(estadio[2:])  # racana
print(estadio[:3])  # mar
print(len(estadio))  # 8

print("m" in estadio)  # True
print("x" not in estadio)  # True
print("m" + "aracana")  # maracana
print("a" * 3)  # aaa

minha_str = "livro python 3"
print(minha_str[13])  # 3

# minha_str[13] = "2"
# Traceback (most recent call last):
#   File "strings.py", line 40, in <module>
#     minha_str[13] = "2"
# TypeError: 'str' object does not support item assignment

minha_str = minha_str[0:13] + "2"
print(minha_str)

minha_str = minha_str.replace("2", "3")
print(minha_str)

print("maracana".capitalize())  # Maracana
print("maracana".count("a"))  # 4
print("maracana".endswith("z"))  # False
print("copa de 2014".split(" "))  # ['copa', 'de', '2014']
print(" ".join(["Copa", "de", "2014"]))  # Copa de 2014
print("copa de 2014".replace("2014", "2018"))  # copa de 2018

print("%d dias para a copa" % (100))  # 100 dias para a copa
print("{} dias para a copa".format(100))  # 100 dias para a copa
print("{dias} dias para a copa".format(dias=100))  # 100 dias para a copa

dias_para_copa = 100
print(f'{dias_para_copa} dias para a copa')  # 100 dias para a copa

print('{:<60}'.format('alinhado à esquerda, ocupando 60 posições'))
print('{:>60}'.format('alinhado à direita, ocupando 60 posições'))
print('{:^60}'.format('centralizado, ocupando 60 posições'))
print('{:.^60}'.format('centralizando ao alterar o caractere de espaçamento'))
