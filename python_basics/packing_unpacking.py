from datetime import date, datetime

# d = (2013, 3, 15)
# print(date(d[0], d[1], d[2]))

# packing
d = (2013, 3, 15)

print(date(*d))


def new_user(active=False, admin=False):
    print(active)
    print(admin)


config = {"active": False, "admin": True}

# new_user(config.get('active'), config.get('admin'))
# packing
new_user(**config)

#  * = tupla
# ** = dictionary


# packing
def unpacking_experiment(*args):
    arg1 = args[0]
    arg2 = args[1]
    others = args[2:]
    print(arg1)
    print(arg2)
    print(others)


unpacking_experiment(1, 2, 3, 4, 5, 6)


def another_unpacking_experiment(**kwargs):
    print(kwargs)


another_unpacking_experiment(named="Test", other="Other")