my_list = [1, 2, 3, 4, 5]
print(my_list)

words = ["salario", "imposto"]
print(words)

another_list = [1, "salario"]
print(another_list)

alist = [[1, 2, 3], "salario", 10]
print(alist)

lista = ['impostos', 'salarios', 'altos', 'baixos']
print(lista)

print(lista[0])
print(lista[-1])
print(lista[2:4])

lista[2] = "Altos"
lista[3] = "Baixos"
print(lista)

lista[0:2] = ["Impostos", "Salários"]
print(lista)

outra_lista = []

if outra_lista:
    print("Nunca sou executado")
else:
    print("Sempre sou executado")