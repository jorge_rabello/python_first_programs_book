imposto = float(input("Digite o imposto: "))
if imposto < 10:
    print("Médio")
elif imposto < 27.5:
    print("Alto")
else:
    print("Muito alto")

imposto = 0.3
print("Alto" if imposto > 0.27 else "Baixo")

imposto = 0.10
print("Alto" if imposto > 0.27 else "Baixo")

valor_imposto = "Alto" if imposto > 0.27 else "Baixo"
print(valor_imposto)